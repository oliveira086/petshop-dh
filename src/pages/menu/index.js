import React from 'react';
import './styles.css';
import cachorroDois from '../../assets/imgs/cachorrodois.jpg'

export default function Menu({ history }) {

    window.onload = function nomeDoPet(){
        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        document.getElementById('nomeDois').value = dadosObject[0].nome
    }

    let quantidade = localStorage.getItem('quantidade')

    if (quantidade == 0){
        window.location.reload()
        quantidade = quantidade +1
        localStorage.setItem('quantidade', quantidade)
    }
    else{

    }
    
    
    function cachorroSelected(){

        document.getElementById('cachorro').style.background = 'lightgreen'
        document.getElementById('cachorro').style.color = 'white'

        document.getElementById('gato').style.background = 'lightyellow'
        document.getElementById('gato').style.color = '#494949'

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].tipo = 'Cachorro'

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)
        
    }

    function gatoSelected(){

        document.getElementById('gato').style.background = 'lightgreen'
        document.getElementById('gato').style.color = 'white'

        document.getElementById('cachorro').style.background = 'lightyellow'
        document.getElementById('cachorro').style.color = '#494949'

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].tipo = 'Gato'

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)

    }

    function machoSelected(){

        document.getElementById('macho').style.background = 'lightgreen'
        document.getElementById('macho').style.color = 'white'

        document.getElementById('femea').style.background = 'lightyellow'
        document.getElementById('femea').style.color = '#494949'

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].genero = 'Macho'

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)

    }

    function femeaSelected(){

        document.getElementById('femea').style.background = 'lightgreen'
        document.getElementById('femea').style.color = 'white'

        document.getElementById('macho').style.background = 'lightyellow'
        document.getElementById('macho').style.color = '#494949'

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].genero = 'Femea'

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)

    }

    function simSelected(){

        document.getElementById('sim').style.background = 'lightgreen'
        document.getElementById('sim').style.color = 'white'

        document.getElementById('nao').style.background = 'lightyellow'
        document.getElementById('nao').style.color = '#494949'

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].vacinado = 'Sim'

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)

    }

    function naoSelected(){

        document.getElementById('nao').style.background = 'lightgreen'
        document.getElementById('nao').style.color = 'white'

        document.getElementById('sim').style.background = 'lightyellow'
        document.getElementById('sim').style.color = '#494949'

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].vacinado = 'Não'

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)

    }
    

    function concluir(){
        let raca = document.getElementById('raca').value
        let idade = document.getElementById('idade').value

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        dadosObject[0].raca = raca
        dadosObject[0].idade = idade

        let dadosString = JSON.stringify(dadosObject)
        localStorage.setItem('pet',dadosString)

        history.push('/servicos');



    }
    
    
    return(
        
        <div className="geralMenu">
            <img className='cachorroDoisImagem' src={cachorroDois}></img>   
            <div className="retanguloCadastro">
                    <div className="nomeDois">
                        <span>Nome:</span>
                        <input id='nomeDois' type='text' value=''></input>
                    </div>
                
                    <div className="tipo">
                        <input id = "cachorro" className="cachorro" type='submit' value='Cachorro' onClick={()=>cachorroSelected()}></input>
                        <input id = "gato" className="gato" type='submit' value='Gato' onClick={()=>gatoSelected()}></input>
                    </div>

                    <div className="raca">
                        <span>Raça:</span>
                        <input id='raca' type='text' placeholder='Qual a raça do seu Pet?'></input>
                    </div>

                    <div className="idade">
                        <span>Nascimento:</span>
                        <input id='idade' type='text' placeholder='Ano de nascimento do seu Pet?'></input>
                    </div>

                    <div className="genero">
                        <span>Genero:</span>
                        <div className="generoGeral">
                            <input id = "macho" className="macho" type='submit' value='Macho' onClick={()=> machoSelected()}></input>
                            <input id = "femea" className="femea" type='submit' value='Femea' onClick={()=> femeaSelected()}></input>
                        </div>
                        
                    </div>

                    <div className="vacinado">
                        <span>Vacinado:</span>
                        <div className="vacinadoGeral">
                            <input id = "sim" className="sim" type='submit' value='Sim' onClick={()=>simSelected()}></input>
                            <input id = "nao" className="nao" type='submit' value='Não' onClick={()=>naoSelected()}></input>
                        </div>
                    </div>

                    <div className="botaoMenu" >
                        <input className="botaoInput" type='submit' value='Concluir' onClick={()=> concluir()}></input>
                    </div>

            </div>
            
        </div>

    )
    }