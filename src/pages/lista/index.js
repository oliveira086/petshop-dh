import React from 'react';
import FlatList from 'flatlist-react'
import './styles.css';

import gatoQuadrado from '../../assets/imgs/gato-quadrado.jpg'
import cachorroQuadrado from '../../assets/imgs/cachorro-quadrado.jpg'

export default function Lista({ history }) {

    function adicionar(){
        document.getElementById('geralOpaco').style.display = 'flex'
        document.getElementById('popUp').style.display = 'flex'

    }

    function fechar(){
        document.getElementById('geralOpaco').style.display = 'none'
    }

    function inserirPet(){
        let nomeDoDonoDoPet = localStorage.getItem('dono')
        let nome = document.getElementById('nomeDoPet').value
        let raca = document.getElementById('raca').value
        let idade = document.getElementById('idade').value
        let tipo = localStorage.getItem('tipo')
        let genero = localStorage.getItem('genero')
        let vacinado = localStorage.getItem('vacina')

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)

        let objeto = {
                dono: nomeDoDonoDoPet,
                nome: nome,
                tipo: tipo,
                raca: raca,
                idade: idade,
                genero: genero,
                vacinado:vacinado,
                servicos: []
            }
        
        dadosObject.push(objeto)

        let dadosString = JSON.stringify(dadosObject)

       
        localStorage.setItem('pet',dadosString)
        window.location.reload()
        
    
        
    }

    function cachorroSelected(){

        document.getElementById('cachorro').style.background = 'lightgreen'
        document.getElementById('cachorro').style.color = 'white'

        document.getElementById('gato').style.background = 'lightyellow'
        document.getElementById('gato').style.color = '#494949'

        localStorage.setItem('tipo','Cachorro')
        
    }

    function gatoSelected(){

        document.getElementById('gato').style.background = 'lightgreen'
        document.getElementById('gato').style.color = 'white'

        document.getElementById('cachorro').style.background = 'lightyellow'
        document.getElementById('cachorro').style.color = '#494949'

        localStorage.setItem('tipo','Gato')
        
    }
    function machoSelected(){

        document.getElementById('macho').style.background = 'lightgreen'
        document.getElementById('macho').style.color = 'white'

        document.getElementById('femea').style.background = 'lightyellow'
        document.getElementById('femea').style.color = '#494949'

        localStorage.setItem('genero','Macho')

    }
    function femeaSelected(){

        document.getElementById('femea').style.background = 'lightgreen'
        document.getElementById('femea').style.color = 'white'

        document.getElementById('macho').style.background = 'lightyellow'
        document.getElementById('macho').style.color = '#494949'

        localStorage.setItem('genero','Femea')

    }

    function simSelected(){

        document.getElementById('sim').style.background = 'lightgreen'
        document.getElementById('sim').style.color = 'white'

        document.getElementById('nao').style.background = 'lightyellow'
        document.getElementById('nao').style.color = '#494949'

        localStorage.setItem('vacina','Sim')

    }
    function naoSelected(){

        document.getElementById('nao').style.background = 'lightgreen'
        document.getElementById('nao').style.color = 'white'

        document.getElementById('sim').style.background = 'lightyellow'
        document.getElementById('sim').style.color = '#494949'

        localStorage.setItem('vacina','Não')

    }

    function listagemDosServicos(lista){

        let item = JSON.stringify(lista)

        localStorage.setItem('item',item)

        document.getElementById('retanguloPetFundo').style.display = 'flex'

        let nomePet = lista.nome
        let dono = lista.dono
        let tipo = lista.tipo
        let genero = lista.genero
        let raca = lista.raca
        let idade = 2020 - lista.idade
        let vacinado = lista.vacinado

        document.getElementById('nomeDono').value = dono
        document.getElementById('nomePet').value = nomePet
        document.getElementById('idadeDetalhes').value = `${idade} anos`
        document.getElementById('racaDetalhes').value = raca
        document.getElementById('tipoDetalhes').value = tipo
        document.getElementById('generoDetalhes').value = genero
        document.getElementById('vacinadoDetalhes').value = vacinado

        fotoPet(lista)

    }

    function fotoPet(lista){

        let tipo = lista.tipo

        if(tipo == 'Cachorro'){
            document.getElementById('petQuadrado').src = cachorroQuadrado
        }
        else{
            document.getElementById('petQuadrado').src = gatoQuadrado
        }
    }

    function removerVisibilidadeDadosPet(){
        document.getElementById('retanguloPetFundo').style.display = 'none'
    }

    function deletarPet(){
        let item = localStorage.getItem('item')
        let dadosObject = localStorage.getItem('pet')

        let indicePet = parseInt(dadosObject.indexOf(item))

        let lista = JSON.parse(dadosObject)

        lista.pop(indicePet)

        localStorage.setItem('pet',JSON.stringify(lista))

        window.location.reload()

        
    }

    
    return(
        
        <div className="geralLista">
            <div className="geralOpaco" id="geralOpaco">
                <div className="popUp" id='popUp'>
                    <input className="fechar" type="submit" value='x' onClick={()=>fechar()}></input>
                    <div className="popUpInterno">
                        <div className="animalDois">
                            <span>Nome do Pet:</span>
                            <input id='nomeDoPet' type='text' placeholder='Qual o nome do seu pet?'></input>
                        </div>

                        <div className="tipo">
                            <input id = "cachorro" className="cachorro" type='submit' value='Cachorro' onClick={()=>cachorroSelected()}></input>
                            <input id = "gato" className="gato" type='submit' value='Gato' onClick={()=>gatoSelected()}></input>
                        </div>

                        <div className="raca">
                            <span>Raça:</span>
                            <input id='raca' type='text' placeholder='Qual a raça do seu Pet?'></input>
                        </div>

                        <div className="idade">
                            <span>Nascimento:</span>
                            <input id='idade' type='text' placeholder='Ano de nascimento do seu Pet?'></input>
                        </div>

                        <div className="genero">
                            <span>Genero:</span>
                            <div className="generoGeral">
                                <input id = "macho" className="macho" type='submit' value='Macho' onClick={()=>machoSelected()}></input>
                                <input id = "femea" className="femea" type='submit' value='Femea' onClick={()=>femeaSelected()}></input>
                            </div>
                        </div>
                        <div className="vacinado">
                            <span>Vacinado:</span>
                            <div className="vacinadoGeral">
                                <input id = "sim" className="sim" type='submit' value='Sim' onClick={()=>simSelected()}></input>
                                <input id = "nao" className="nao" type='submit' value='Não' onClick={()=>naoSelected()}></input>
                            </div>
                        </div>
                        <div className="botaoMenu" >
                            <input className="botaoInput" type='submit' value='Adicionar' onClick={()=>inserirPet()}></input>
                        </div>
                        
                    </div>

                </div>
            </div>

            <div className="retaguloFundo">
                <div className="retanguloListagem"> 
                    <div className="retanguloScroll">
                        <FlatList list={JSON.parse(localStorage.getItem('pet'))} renderItem={item =>
                        <input type="submit" className="itemPet" value={item.nome} onClick={()=>listagemDosServicos(item)}></input>}/>
                    </div>

                </div>
                    <div className="divBotao">
                        <input className="botaoAdicionarPet" type="submit" value="Adicionar Pet" onClick={()=>adicionar()}></input>
                    </div>
            </div>

            <div className="retanguloPetFundo" id="retanguloPetFundo">
                <div className="retanguloPet">

                    <div className="circuloIcone">
                        <img className='petQuadrado' id="petQuadrado" src={cachorroQuadrado}></img>
                    </div>

                    <div className="retanguloDados">
                        <div className="botaoFechar">
                            <input type="submit" id="botaoFechar" value='X'
                            onClick={()=>removerVisibilidadeDadosPet()}></input>
                        </div>
                        <div className="rowDados">
                            <span>{'Dono(a):'}</span>
                            <input type="submit" id="nomeDono" value=''></input>
                        </div>

                        <div className="rowDados">
                            <span>Pet:</span>
                            <input type="submit" id="nomePet" value=''></input>
                        </div>

                        <div className="rowDados">
                            <span>Idade:</span>
                            <input type="submit" id="idadeDetalhes" value=''></input>
                        </div>

                        <div className="rowDados">
                            <span>Raca:</span>
                            <input type="submit" id="racaDetalhes" value=''></input>
                        </div>

                        <div className="rowDados">
                            <span>Tipo:</span>
                            <input type="submit" id="tipoDetalhes" value=''></input>
                        </div>
                        
                        <div className="rowDados">
                            <span>Genero:</span>
                            <input type="submit" id="generoDetalhes" value=''></input>
                        </div>

                        <div className="rowDados">
                            <span>{'Vacinado(a):'}</span>
                            <input type="submit" id="vacinadoDetalhes" value=''></input>
                        </div>

                        
                        <input type="submit" id="deletarPet" value='Deletar Pet' onClick={()=>deletarPet()}></input>
                        
                        
                        
                        
                        
                       

                    </div>
                    

                </div>
 
            </div>

            

            

            
        </div>

    )
    }