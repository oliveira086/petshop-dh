import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Login from './pages/login';
import Menu from './pages/menu'
import Servicos from './pages/servicos'
import Lista from './pages/lista'


export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path='/menu' exact component={Menu}/>
                <Route path='/servicos' exact component={Servicos}/>
                <Route path='/lista' exact component={Lista}/>
            </Switch>
        </BrowserRouter>
    );
}
